## happy path
* greet
  - utter_greet
* mood_great
  - utter_happy

## sad path 1
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* affirm
  - utter_happy

## sad path 2
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* deny
  - utter_goodbye
  - action_restart

## say goodbye
* goodbye
  - utter_goodbye
  - action_restart

## bot challenge
* bot_challenge
  - utter_iamabot
  - action_restart

## New Story

* greet
    - utter_greet
* mood_great
    - utter_happy
    - utter_continue
* textNewVariable
    - utter_textContinueVariable
* newVariable{"variable":"x","description":"casas derruidas"}
    - utter_newVar
    - utter_continue
* getHelp
    - action_get_help
    - utter_continue
* goodbye

## New Story

* greet
    - utter_greet
    - utter_continue
* textNewVariable
    - utter_textContinueVariable
* newVariable{"variable":"x","description":"numero de amigos"}
    - utter_newVar
    - utter_continue
* getHelp
    - action_get_help
    - utter_continue
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_greet
* affirm
    - utter_happy
    - utter_continue
* textNewVariable
    - utter_textContinueVariable
* newVariable
    - utter_newVar
    - utter_continue

## New Story

* textNewVariable
    - utter_textContinueVariable
* newVariable{"variable":"x","description":"artilugios en la caja"}
    - utter_newVar
    - utter_continue
* getHelp
    - action_get_help
    - utter_continue
* getVariable{"variable":"x"}

## New Story
* greet
    - utter_greet
* affirm
    - utter_happy
    - utter_continue
* textNewVariable
    - utter_textContinueVariable
* newVariable{"variable":"r","description":"numero de pollos"}
    - utter_newVar
    - utter_continue
* textNewVariable
    - utter_textContinueVariable
* newVariable{"variable":"v","description":"amigos vivos"}
    - utter_newVar
    - utter_continue

## New Story

* greet
    - utter_greet
* mood_great
    - utter_happy
* newVariable{"variable":"x","description":"numero de amigos"}
    - utter_newVar
    - utter_continue
* newVariable{"description":"numero de amigos","variable":"x"}
    - utter_newVar
    - utter_continue
* getVariable{"variable":"x"}

## New Story

* greet
    - utter_greet
* mood_great
    - utter_happy
    - utter_continue
* textNewVariable
    - utter_textContinueVariable
* newVariable{"description":"hay en la casa","variable":"m"}
    - utter_newVar
    - utter_continue
* goodbye

## New Story

* greet
    - utter_greet
* mood_great
    - utter_happy
    - utter_continue
* newVariable{"variable":"x","description":"naranjas en la caja grande"}
    - utter_newVar
    - utter_continue

## New Story

* greet
    - utter_greet
* affirm
    - utter_happy
    - utter_continue
* textNewNumber
    - utter_utter_textContinueNumber
* newNumber{"description":"numero de amigos","number":"6"}
    - slot{"description":"numero de amigos"}
    - slot{"number":"6"}
    - utter_newNumber
    - utter_continue

## New Story

* greet
    - utter_greet
* affirm
    - utter_happy
    - utter_continue
* newVariable{"variable":"x","description":"libros de carla"}
    - slot{"description":"libros de carla"}
    - slot{"variable":"x"}
    - utter_newVar
    - utter_continue
* expression{"expression":"5x+4=7"}
    - slot{"description":"libros de carla"}
    - slot{"variable":"x"}
    - utter_textContinueVariable
    - slot{"description":"libros de carla"}
    - slot{"variable":"x"}
    - slot {"expression":"5x+4=7"}
* getHelp
    - slot{"description":"libros de carla"}
    - slot{"variable":"x"}
    - action_get_help
    - utter_continue
* getHelp
    - action_get_help
    - utter_continue
* getHelp
    - action_get_help
    - utter_continue

## New Story

* getHelp
    - action_get_help
    - utter_continue

## New Story

* expression{"expression":"32y+r"}
    - utter_textContinueVariable
* newVariable{"variable":"x","description":"numero de amigos"}
    - slot{"description":"numero de amigos"}
    - slot{"variable":"x"}
    - utter_newVar
    - utter_continue
    - slot{"expression":"32y+r"}
* newVariable{"variable":"f","description":"naranjas en el saco"}
    - slot{"description":"naranjas en el saco"}
    - slot{"variable":"f"}
    - utter_newVar
    - utter_continue
    - slot{"description":"numero de amigos"}
    - slot{"variable":"x"}
    - slot{"description":"naranjas en el saco"}
    - slot{"variable":"f"}
* newVariable{"variable":"g"}
    - slot{"variable":"g"}

## New Story

* newVariable{"description":"perros y gatos"}
    - slot{"description":"perros y gatos"}
    - utter_newVar
    - utter_continue
* textNewVariable{"variable":"x"}
    - slot{"variable":"x"}
    - utter_textContinueVariable
* textNewVariable
    - utter_textContinueVariable
* textNewVariable
    - utter_textContinueVariable
* textNewVariable
    - utter_textContinueVariable
* textNewVariable
    - utter_textContinueVariable

## New Story

* resetEntities
    - slot{"description":null}
    - slot{"variable":null}
    - utter_continue
* resetExpression
    - action_resetExpression
    - slot{"expression":null}
    - utter_continue

## New Story

* newVariable{"description":"perros y gatos"}
    - slot{"description":"perros y gatos"}
    - utter_newVar
    - utter_continue
* resetEntities
    - slot{"description":null}
    - slot{"variable":null}
    - utter_continue
* resetExpression
    - action_resetExpression
    - slot{"expression":null}
    - utter_continue

## New Story

* greet
    - utter_greet
* mood_great
    - utter_happy
    - utter_continue
* textNewVariable{"variable":"x"}
    - slot{"variable":"x"}
    - utter_textContinueVariable
* newVariable{"description":"numero de amigos"}
    - slot{"description":"numero de amigos"}
    - utter_newVar
    - utter_continue
    - slot{"variable":"x"}
    - slot{"description":"numero de amigos"}
* resetEntities
    - action_resetEntities
    - slot{"description":null}
    - slot{"variable":null}
* resetExpression
    - action_resetExpression
    - slot{"expression":null}
* getVariable

## New Story

* resetEntities
    - action_resetEntities
* resetEntities
    - action_resetEntities
* resetExpression
    - action_resetExpression
* textNewVariable{"variable":"x"}
    - slot{"variable":"x"}
    - utter_textContinueVariable
* resetEntities
    - slot{"variable":"x"}
    - action_resetEntities
    - slot{"variable":"x"}
    - slot{"description":"numero de amigos"}
    - slot{"variable":"x"}
    - slot{"variable":"x"}
    - action_resetEntities
    - slot{"variable":null}
* resetExpression
    - slot{"expression":null}
    - action_resetExpression
* newVariable{"variable":"x","description":"numero de amigos"}
    - slot{"description":"numero de amigos"}
    - slot{"variable":"x"}
    - utter_newVar
    - utter_continue
* resetEntities
    - action_resetEntities
    - slot{"variable":null}
    - slot{"description":null}

## New Story

* greet
    - utter_greet
* affirm
    - utter_happy
    - utter_continue
* textNewVariable{"variable":"x"}
    - slot{"variable":"x"}
    - utter_textContinueVariable
* textNewVariable{"variable":"y"}
    - slot{"variable":"y"}
    - utter_textContinueVariable
* newVariable{"description":"numero de gatos"}
    - slot{"description":"numero de gatos"}
    - utter_newVar
    - utter_continue
* resetEntities
    - action_resetEntities
    - slot{"variable":null}
    - slot{"description":null}
* resetExpression
    - action_resetExpression
    - slot{"expression":null}

## New Story

* resetExpression
    - utter_continue
* resetExpression
    - action_resetExpression
    - slot{"expression":null}
    - utter_continue

## New Story

* expression{"expression":"5x+30"}
    - slot{"expression":"5x+30"}
    - utter_textContinueVariable
* resetExpression
    - action_resetExpression
    - slot{"expression":null}
    - utter_continue
