# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

# from typing import Any, Text, Dict, List
#
from rasa_sdk.events import SlotSet
from rasa_sdk.events import AllSlotsReset
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action


class AllSlotsReset(Action):
	def name(self):
		return 'action_resetEntities'
	def run(self, dispatcher, tracker, domain):
		#AllSlotsReset()
		return [SlotSet("variable", None), SlotSet("description", None)]



class ResetExpression(Action):
	def name(self):
		return 'action_resetExpression'
	def run(self, dispatcher, tracker, domain):
		#AllSlotsReset()
		return [SlotSet("expression", None)]
